import React, { useEffect, useRef, useState } from 'react';

interface TaskEntity {
    name: string;
    isCompleted: boolean;
}

type Filter = 'all' | 'completed' | 'active'



export const Todo: React.FC = () => {
    const [tasks, setTasks] = React.useState<TaskEntity[]>([]);
    const [filter, setFilter] = React.useState<Filter>('all');

    const inputRef = useRef<HTMLInputElement>(null);

    function handleKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
        if (!inputRef.current) {
            return;
        }

        const inputValue = inputRef.current.value;

        if (Boolean(inputValue) && event.key === 'Enter') {
            handleCreateTask(inputValue)
            inputRef.current.value = '';
        }
    }

    function handleCreateTask(name: string) {
        setTasks([...tasks, {name, isCompleted: false}]);
    }

    function handleDeleteTask(index: number) {
        const tasksCopy = [...tasks];
        tasksCopy.splice(index, 1);
        setTasks(tasksCopy);
    }

    function handleUpdateTask(newTask: TaskEntity, index: number) {
        const tasksCopy = [...tasks];
        tasksCopy.splice(index, 1, newTask);
        setTasks(tasksCopy);
    }

    function filterTask(task: TaskEntity) {      
        if (filter === 'all') {
            return true;
        }

        if (filter === 'completed') {
            return task.isCompleted;
        }  
        
        if (filter === 'active') {
            return !task.isCompleted;
        }
    }

    function handleClearCompleted(){
        setTasks(tasks.filter(task => !task.isCompleted));
    }

    return (
        <>
            <header>
                <h1>todos</h1>
            </header>

            <input
                type="text"
                placeholder="What needs to be done?"
                ref={inputRef}
                onKeyPress={handleKeyPress}
            />

            <ul>
                {tasks.filter(filterTask).map((task, index) => (
                    <li key={`${index}-${task}`}>
                        <Task
                            task={task} 
                            onClick={() => handleDeleteTask(index)} 
                            onUpdateTask={(newTask: TaskEntity) => handleUpdateTask(newTask, index)}
                            onDeleteTask={() => handleDeleteTask(index)}
                        />
                    </li>))}
            </ul>
            <div>  
                <span>{tasks.filter(task => !task.isCompleted).length} items left</span>                
                <button onClick={() => setFilter('all')}>All</button>
                <button onClick={() => setFilter('active')}>Active</button>
                <button onClick={() => setFilter('completed')}>Completed</button>
                <button onClick={handleClearCompleted}>Clear completed</button>
            </div>
        </>
    );
}

interface TaskProps {
    task: TaskEntity;
    onClick: () => void;
    onUpdateTask: (newTask: TaskEntity) => void;
    onDeleteTask: () => void;
}

const Task: React.FC<TaskProps> = ({task, onClick, onUpdateTask, onDeleteTask }) => {
    const [inputValue, setInputValue] = useState(task.name);
    const [isEditing, setIsEditing] = useState(false);

    const taskRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const taskElement = taskRef.current;
        
        if (!taskElement) {
            return;
        }

        function handleEdit() {
            setIsEditing(true);
        }

        taskElement.addEventListener('dblclick', handleEdit);

        return () => taskElement.removeEventListener('dblclick', handleEdit);
    })

    function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
        setInputValue(event.target.value);
    }

    function handleKeyPress(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.key !== 'Enter') {
            return;
        }

        if (!Boolean(inputValue)) {
            onDeleteTask();
            return
        }

        onUpdateTask({...task, name: inputValue});
        setIsEditing(false);
    }

    return (
        <>
            {isEditing ?
                (<input
                    type="text"
                    value={inputValue}
                    onChange={handleChange}
                    onKeyPress={handleKeyPress}
                />)
                :
                (<div ref={taskRef}>
                    <input 
                        type="checkbox" 
                        checked={task.isCompleted} 
                        onChange={() => onUpdateTask({...task, isCompleted: !task.isCompleted})} 
                    />
                    {task.name}
                    <button onClick={onClick}> x </button>
                </div>)
            }
        </>
    )
}