import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Todo } from '../Todo';

describe('La aplicación Todo', () => {
  it('renderiza el componente', () => {
    render(<Todo />);
    
    const Titulo = screen.getByText(/todos/i);
    
    expect(Titulo).toBeInTheDocument();
  })

  it('Cuando se escribe una nueva tarea, se lista', () => {
    render(<Todo />);
    
    const input = screen.getByPlaceholderText(/What needs to be done?/);
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');
   
    expect(screen.getByText(/new task/)).toBeInTheDocument();
  })

  it('Cuando se escribe una tarea nueva no se lista hasta que no se pulsa Enter',() => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/);
    userEvent.type(input, 'new task');

    expect(screen.queryByText(/new task/)).not.toBeInTheDocument();
  })

  it('Cuando se escribe una tarea nueva y se pulsa enter se elimina el valor del input',() => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    expect(input.value).toBe('');
  })

  it('Cuando se añade más de una tarea, se listan todas', () => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    userEvent.type(input, 'another task');
    userEvent.type(input, '{enter}');

    expect(screen.getByText(/new task/)).toBeInTheDocument();
    expect(screen.getByText(/another task/)).toBeInTheDocument();
  })

  it('Cuando se clicka en la X de una tarea, se elimina de la lista', () => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');
    userEvent.type(input, 'another task');
    userEvent.type(input, '{enter}');
    const xButton = screen.getAllByRole('button', {name: 'x'});
    
    userEvent.click(xButton[0]);

    expect(screen.getByText(/another task/)).toBeInTheDocument();
    expect(screen.queryByText(/new task/)).not.toBeInTheDocument();
  })


  it('Cuando se hace doble click en una tarea, permite editarlo',() => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    const newTask = screen.getByText(/new task/); 
    userEvent.dblClick(newTask);

    const newTaskInput = screen.getByDisplayValue(/new task/);
    userEvent.clear(newTaskInput);
    userEvent.type(newTaskInput, 'changed task');
    userEvent.type(newTaskInput, '{enter}');

    expect(screen.queryByText(/new task/)).not.toBeInTheDocument();
    expect(screen.getByText(/changed task/)).toBeInTheDocument();
  })

  it('Cuando se hace doble click en una tarea y se introduce "", se borra la tarea de la lista', () => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    const newTask = screen.getByText(/new task/); 
    userEvent.dblClick(newTask);

    const newTaskInput = screen.getByDisplayValue(/new task/);
    userEvent.clear(newTaskInput);
    userEvent.type(newTaskInput, '{enter}');
    
    expect(newTaskInput).not.toBeInTheDocument();
    expect(screen.queryByText(/new task/)).not.toBeInTheDocument();
  })

  it('Cuando se clicka en el check de una tarea y se filtra por Activas, ésta no se muestra', () => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    const taskCheckBox = screen.getByRole('checkbox', {checked: false});
    userEvent.click(taskCheckBox);

    expect(screen.getByText(/new task/)).toBeInTheDocument();

    const activeButton = screen.getByRole('button', {name: 'Active'});
    userEvent.click(activeButton);

    expect(screen.queryByText(/new task/)).not.toBeInTheDocument();
  })

  it('Cuando se clicka en el botón "Clear completed", se borran las tareas completas de la lista', () => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    const taskCheckBox = screen.getByRole('checkbox', {checked: false});
    userEvent.click(taskCheckBox);

    const activeButton = screen.getByRole('button', {name: 'Clear completed'});
    userEvent.click(activeButton);

    expect(screen.queryByText(/new task/)).not.toBeInTheDocument();
  })

  it('Cuando se añade una tarea, se muestra el número de tareas pendientes', () => {
    render(<Todo />);

    const input = screen.getByPlaceholderText(/What needs to be done?/) as HTMLInputElement;
    userEvent.type(input, 'new task');
    userEvent.type(input, '{enter}');

    userEvent.type(input, 'another task');
    userEvent.type(input, '{enter}');

    expect(screen.queryByText(/2 items left/)).toBeInTheDocument();
  })
});
